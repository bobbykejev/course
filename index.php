<?php
/**
 * First php Homework 20.10.2016
 */


/* 4. Largest Number Again
Having in mind the considerations in the Task 3, find the largest number, but this time the input numbers might contain negatives. */

$arrayNum = [3, 7, -4, 5];
$stringNum = implode(', ', $arrayNum);

echo "The greatest number from numbers $stringNum is: " . max($arrayNum) . '<br />';

$arrayNum = [-3, -2, -17, -33, -1];
$stringNum = implode(', ', $arrayNum);

echo "The greatest number from numbers $stringNum is: " . max($arrayNum);

/* 5. Count Letters
You will receive a single line from the standard input containing a word (or at least a set or characters).
You need to print on the standard input how many times each letter is found in order of the letter appearance,
in format {letter} -> {times} */

$word = 'apple';
$lettersArr = str_split($word);
$resultArr = [];

foreach ($lettersArr as $key => $letter) {
    if (!array_key_exists($letter, $resultArr)) {
        $resultArray[$letter] = 0;
    }
    $resultArr[$letter]++;
}

foreach ($resultArr as $k => $v) {
    echo $k . ' -> ' . $v . '<br />';
}

/* 6. Count Letters – Sorted
As in Task 5, but the output should be sorted by the times a letter has occurred in descending order,
then in order of appearance. */

$word = 'apple';
$lettersArr = str_split($word);
$resultArr = [];

foreach ($lettersArr as $key => $letter) {
    if (!array_key_exists($letter, $resultArr)) {
        $resultArray[$letter] = 0;
    }
    $resultArr[$letter]++;
}
arsort($resultArr);

foreach ($resultArr as $k => $v) {
    echo $k . ' -> ' . $v . '<br />';
}