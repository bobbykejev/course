<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<div class="container">
    <div class="col-md-10 col-md-offset-1">
        <form method="get">
            <div>
                Delimiter:
                <select name="delimiter">
                    <option value=",">,</option>
                    <option value="|">|</option>
                    <option value="&">&</option>
                </select>
            </div>
            <div>
                Names:
                <input type="text" name="names"/>
            </div>
            <div>
                Ages:
                <input type="text" name="ages"/>
            </div>
            <div>
                Records Per Page:
                <select name="itemsPerPage">
                    <option value="3">3</option>
                    <option value="6">6</option>
                    <option value="9">9</option>
                </select>
            </div>
            <div>
                Show only over 18 years old:
                <input type="checkbox" name="over18">
            </div>
            <div>
                <input type="submit" name="filter" value="Filter!"/>
            </div>
        </form>
        <table class="table table-striped table-condensed table-bordered table-rounded">
            <thead>
                <tr>
                    <th>Names</th>
                    <th>Ages</th>
                </tr>
            </thead>
            <tbody>
                <?php while ($row = mysqli_fetch_assoc($result)): ?>
                    <tr>
                        <td width="20%"><?= $row['Name']; ?></td>
                        <td width="20%"><?= $row['Age']; ?></td>
                    </tr>
                <?php endwhile; ?>
            </tbody>
        </table>
        <!-- TODO: Add over18 in the link -->
        <div class="align-center">
            <?= $prevPage; ?>
            <?php for ($i = 1; $i <= $totalPages; $i++): ?>
                <?php ($page == $i) ? $class = "active" : $class = ""; ?>
            <a href='?itemsPerPage=<?= $recordsPerPage; ?>&page=<?= $i; ?>' class="<?= $class; ?>">[<?= $i; ?>]</a>
            <?php endfor; ?>
            <?= $nextPage; ?>
        </div>
    </div>
</div>
