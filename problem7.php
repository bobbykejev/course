<?php

if (isset($_GET['filter'])) {
    $delimiter = $_GET['delimiter'];
    $names = trim(
        preg_replace('/ +/', ' ',
            preg_replace('/[^A-Za-z0-9 ]/', ' ',
                urldecode(
                    html_entity_decode(
                        strip_tags(
                            $_GET['names']
                        )
                    )
                )
            )
        )
    );
    $ages = trim(
        preg_replace('/ +/', ' ',
            preg_replace('/[^A-Za-z0-9 ]/', ' ',
                urldecode(
                    html_entity_decode(
                        strip_tags(
                            $_GET['ages']
                        )
                    )
                )
            )
        )
    );

    // Insert into db
    if (!empty($names) && (!empty($ages))) {
        $connection = mysqli_connect('127.0.0.1','bobby','123456', 'students');

        if (!$connection) {
            die("Connection failed: " . mysqli_connect_error());
        }

        $query = "INSERT INTO students (Name, Age)
                  VALUES (" . "'" . $names . "'" . ",$ages)";

        if (mysqli_query($connection, $query)) {
            echo "<p style='color: green;'>New record created successfully</p>";
        } else {
            echo "Error: " . $query . "<br>" . mysqli_error($connection);
        }
        mysqli_close($connection);
    }
}

// Get data from db
$connection = mysqli_connect('127.0.0.1','bobby','123456', 'students');

if (!$connection) {
    die("Connection failed: " . mysqli_connect_error());
}

if (isset($_GET["itemsPerPage"])) {
    $recordsPerPage  = $_GET["itemsPerPage"];
} else {
    $recordsPerPage = 3;
}

if (isset($_GET["page"])) {
    $page  = $_GET["page"];
} else {
    $page = 1;
}

$startFrom = ($page-1) * $recordsPerPage;


if (isset($_GET['over18'])) {
    $query = "SELECT * FROM students WHERE Age >= 18";
} else {
    $query = "SELECT * FROM students LIMIT $startFrom, $recordsPerPage";
}

$result = mysqli_query ($connection, $query);

$totalRecords = "SELECT * FROM students";
$totalResult = mysqli_query($connection, $totalRecords);
$totalRecords = mysqli_num_rows($totalResult);
$totalPages = ceil($totalRecords / $recordsPerPage);


/* TODO: Add over18 in the link */
$prevPage =  ($page > 1) ? '<a href="?itemsPerPage=' . $recordsPerPage . '&page=' . ($page - 1) . '" title="Previous page">[Previous]</a>' : '<span style="display: none;">[Previous]</span>';
$nextPage = ($page < $totalPages) ? '<a href="?itemsPerPage=' . $recordsPerPage . '&page=' . ($page + 1) . '" title="Next page">[Next]</a>' : '<span style="display: none;">[Next]</span>';

if (!mysqli_query($connection, $query)) {
    echo "Error: " . $query . "<br>" . mysqli_error($connection);
}
mysqli_close($connection);


include_once 'problem7-frontend.php';